# Hebrew translation for kubuntu-notification-helper
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the kubuntu-notification-helper package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
# SPDX-FileCopyrightText: 2023 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: kubuntu-notification-helper\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:39+0000\n"
"PO-Revision-Date: 2023-11-29 22:38+0200\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-04-01 12:20+0000\n"
"X-Generator: Lokalize 23.08.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: src/kcmodule/notificationhelperconfigmodule.cpp:53
#, kde-format
msgid "Kubuntu Notification Helper Configuration"
msgstr "הגדרות מסייע ההתראות של קובונטו"

#: src/kcmodule/notificationhelperconfigmodule.cpp:57
#, kde-format
msgid "(C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter"
msgstr "(C) 2009‏-2010 ג׳ונתן תומס, (C) 2009‏-2014 הראלד סיטר"

#: src/kcmodule/notificationhelperconfigmodule.cpp:59
#, kde-format
msgid "Jonathan Thomas"
msgstr "ג׳ונתן תומס"

#: src/kcmodule/notificationhelperconfigmodule.cpp:60
#, kde-format
msgid "Harald Sitter"
msgstr "הראלד סיטר"

#: src/kcmodule/notificationhelperconfigmodule.cpp:65
#, kde-format
msgid "Configure the behavior of Kubuntu Notification Helper"
msgstr "הגדרות ההתנהגות של מסייע ההתראות של קובונטו"

#: src/kcmodule/notificationhelperconfigmodule.cpp:72
#, kde-format
msgid "Show notifications for:"
msgstr "הצגת התראות עבור:"

#: src/kcmodule/notificationhelperconfigmodule.cpp:74
#, kde-format
msgid "Application crashes"
msgstr "קריסות יישומים"

#: src/kcmodule/notificationhelperconfigmodule.cpp:75
#, kde-format
msgid "Proprietary Driver availability"
msgstr "זמינות מנהלי התקנים קנייניים"

#: src/kcmodule/notificationhelperconfigmodule.cpp:76
#, kde-format
msgid "Upgrade information"
msgstr "פרטי שדרוג"

#: src/kcmodule/notificationhelperconfigmodule.cpp:77
#, kde-format
msgid "Restricted codec availability"
msgstr "זמינות של מפענחים מוגבלים"

#: src/kcmodule/notificationhelperconfigmodule.cpp:78
#, kde-format
msgid "Incomplete language support"
msgstr "תמיכה חלקית בשפה"

#: src/kcmodule/notificationhelperconfigmodule.cpp:79
#, kde-format
msgid "Required reboots"
msgstr "דרישת הפעלה מחדש"

#: src/kcmodule/notificationhelperconfigmodule.cpp:91
#, kde-format
msgid "Notification type:"
msgstr "סוג התראה:"

#: src/kcmodule/notificationhelperconfigmodule.cpp:95
#, kde-format
msgid "Use both popups and tray icons"
msgstr "להשתמש גם בהתראות קופצות וגם בסמלי מגש מערכת"

#: src/kcmodule/notificationhelperconfigmodule.cpp:97
#, kde-format
msgid "Tray icons only"
msgstr "סמלי מגש מערכת בלבד"

#: src/kcmodule/notificationhelperconfigmodule.cpp:99
#, kde-format
msgid "Popup notifications only"
msgstr "התראות קופצות בלבד"
